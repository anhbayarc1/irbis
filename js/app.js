VANTA.DOTS({
    el: "#slider",
    mouseControls: true,
    touchControls: true,
    minHeight: 200.00,
    minWidth: 200.00,
    scale: 1.00,
    scaleMobile: 1.00,
    color: 0x1c00a2,
    backgroundColor: 0xffffff,
    showLines: false,

})
$(document).ready(function () {
    setTimeout(function () {
        if (!Cookies.get('modalShown')) {
            console.log('testing')
            $("#sltc-modal").modal('show');
            Cookies.set('modalShown', true);
        }

    }, 3000);
});
$(document).ready(function () {
    $('.options .oasis').click(function (e) {
        e.preventDefault();
        window.open('https://oasis.irbis.sg', '_blank');
        $('#letusknow').modal('toggle');
    });
    $('.options .typeform').click(function (e) {
        e.preventDefault();
        window.open('https://irbisoasis.typeform.com/to/D4goOD', '_blank');
        $('#letusknow').modal('toggle');
    });
    $('.options .typeform2').click(function (e) {
        e.preventDefault();
        window.open('https://irbisoasis.typeform.com/to/lfLWRt', '_blank');
        $('#letusknow').modal('toggle');
    });
});
function openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

/* Set the width of the sidebar to 0 and the left margin of the page content to 0 */
function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
}